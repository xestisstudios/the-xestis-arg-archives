window.onload = function() {
    var dayToOpen = getCookie('openDay');
    if (dayToOpen == 1) {
        toggleAccordian('day1Accordion');
        location.href = "#day1Box";
    } else if ( dayToOpen == 2) {
        toggleAccordian('day2Accordion');
        location.href = "#day2Box";
    } else if ( dayToOpen == 3) {
        toggleAccordian('day3Accordion');
        location.href = "#day3Box";
    } else if ( dayToOpen == 4) {
        toggleAccordian('day4Accordion');
        location.href = "#day4Box";
    }
}

function onSiteLoad() {
    if (screen.width <= 1088) {
        window.location.replace("../archives.xestis.com/index.html");
    } else {
        var siteURL = window.location.href;
        var cvisit = getCookie('visited');
        if (!cvisit) {
            setCookie('visited', 'true', 7);
            window.location.replace("../archives.xestis.com/index.html");
        }
    }
    setTimeout( function() {
        onSiteLoad();
    }, 300000);
};

function toggleAccordian(elementID){

    //first
    var accordionID = document.getElementById(elementID);
    accordionID.classList.toggle("active");
    var panel = accordionID.nextElementSibling;
    if (panel.style.maxHeight){
        panel.style.maxHeight = null;
    } else {
        panel.style.maxHeight = panel.scrollHeight + "px";
    } 

    //second
    var accordionIDText = accordionID.innerHTML;
    if (accordionIDText.endsWith("+")) {
        accordionIDText = accordionIDText.replace("+", "<span class=\"has-text-weight-bold\">-<span>");
    } else {
        accordionIDText = accordionIDText.replace("<span class=\"has-text-weight-bold\">-<span>", "+");          
    }
    accordionID.innerHTML = accordionIDText;
   
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

/* Function not in use at the moment. */
function deleteCookie(cname) {
    setCookie(cname, "[DELEATED]", -6355);
}