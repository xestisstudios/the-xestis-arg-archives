function onSiteLoad() {
    if (screen.width <= 1088) {
        window.location.replace("../archives.xestis.com/index.html");
    } else {
        var siteURL = window.location.href;
        var cvisit = getCookie('visited');
        if (!cvisit) {
            setCookie('visited', 'true', 7);
            window.location.replace("../archives.xestis.com/index.html");
        }
    }
    setTimeout( function() {
        onSiteLoad();
    }, 300000);
};

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

/* Function not in use at the moment. */
function deleteCookie(cname) {
    setCookie(cname, "[DELEATED]", -6355);
}