// left: 37, up: 38, right: 39, down: 40,
// spacebar: 32, pageup: 33, pagedown: 34, end: 35, home: 36
var keys = {37: 1, 38: 1, 39: 1, 40: 1};

var sites = [
    'http://www.xestis.com/', 
    'http://ithurts.xestis.com/', 
    'http://fun.xestis.com/', 
    'http://suicide.xestis.com/', 
    'http://red.xestis.com/', 
    'http://thenote.xestis.com/', 
    'http://intro.xestis.com/', 
    'http://story.xestis.com/', 
    'http://www.amelia.xestis.com/', 
    'https://dan.xestis.com/', 
    'http://www.hang.xestis.com', 
    'http://emily.xestis.com/',
];

window.onload = function() {
    var cvisit = getCookie('agreed');
    if (!cvisit) {
        modelToggleOn('warningModel');
    }
}

function getURL(pageNum) {
    return sites[pageNum];
}

function getDate() {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();
    if(dd<10) {
        dd = '0'+dd
    } 
    if(mm<10) {
        mm = '0'+mm
    } 
    return yyyy + mm + dd
}

function reset() {
    document.getElementById("ddOutput").innerHTML = " ";
}

function modelToggleOn(modelID) {
    document.getElementById(modelID).classList.add('is-active');
    disableScroll();
    window.scrollTo(0, 0);
}

function modelToggleOff(modelID) {
    setCookie('agreed', 'true', 7);
    setCookie('visited', 'true', 7);
    document.getElementById(modelID).classList.remove('is-active');
    enableScroll();
}

function closeWindow() {
    window.location.replace("https://www.xestis-studios.com/");
}

function preventDefault(e) {
  e = e || window.event;
  if (e.preventDefault)
      e.preventDefault();
  e.returnValue = false;  
}

function preventDefaultForScrollKeys(e) {
    if (keys[e.keyCode]) {
        preventDefault(e);
        return false;
    }
}

function disableScroll() {
  if (window.addEventListener) // older FF
      window.addEventListener('DOMMouseScroll', preventDefault, false);
  window.onwheel = preventDefault; // modern standard
  window.onmousewheel = document.onmousewheel = preventDefault; // older browsers, IE
  window.ontouchmove  = preventDefault; // mobile
  document.onkeydown  = preventDefaultForScrollKeys;
}

function enableScroll() {
    if (window.removeEventListener)
        window.removeEventListener('DOMMouseScroll', preventDefault, false);
    window.onmousewheel = document.onmousewheel = null; 
    window.onwheel = null; 
    window.ontouchmove = null;  
    document.onkeydown = null;  
}

function getJSONP(url, success) {

    var ud = '_' + +new Date,
        script = document.createElement('script'),
        head = document.getElementsByTagName('head')[0] 
               || document.documentElement;

    window[ud] = function(data) {
        head.removeChild(script);
        success && success(data);
    };

    script.src = url.replace('callback=?', 'callback=' + ud);
    head.appendChild(script);

}

function getTheJson(cURL) {
    var returnData;
    getJSONP(cURL, function(data){
        returnData = data;
    });  
    return returnData;
}

function Get(yourUrl){
    var Httpreq = new XMLHttpRequest(); // a new request
    Httpreq.open("GET",yourUrl,false);
    Httpreq.send(null);
    return Httpreq.responseText;          
}

function latestWMSite(xestisURL) {
    wmXestisURL = 'http://archive.org/wayback/available?url=' + xestisURL;
    var xestisJSON = JSON.parse(Get(wmXestisURL));
    var returnArray = [xestisJSON.archived_snapshots.closest.url, parseDate(xestisJSON.archived_snapshots.closest.timestamp)];
    return returnArray;
} 

function parseDate(thisDate) {
    return thisDate.substring(6, 8) + "/" +
           thisDate.substring(4, 6) + "/" +
           thisDate.substring(0, 4);
}

function setSite(pageNum) {
    var site = getURL(pageNum);
    var wmResponse = ["https://web.archive.org/web/" + getDate() + "/" + site, parseDate(getDate())];//latestWMSite(site);
    var wmSite = wmResponse[0];
    var wbDate = wmResponse[1];
    document.getElementById("ddOutput").innerHTML = 
        "<ul>" +
            "<li>As they are now: <a href=\"" + site + "\" target=\"_blank\">" + site + "</a></li>" + 
            "<br>" +
            "<li>From The Internet Archives (Wayback Machine Project). Last recorded entry: " + wbDate + "</li>" +
            "<li>" +
                "<ul>" +
                    "<li>All: <a href=\"https://web.archive.org/web/*/" + site + "\" target=\"_blank\">https://web.archive.org/web/*/" + site + "</a></li>" +
                    "<li>Latest: <a href=\"" + wmSite + "\" target=\"_blank\">" + wmSite + "</a></li>" +
                    "<li>Save: <a href=\"https://web.archive.org/save/" + site + "\" target=\"_blank\">https://web.archive.org/save/" + site + "</a></li>" +
                "</ul>" +
            "</li>" +
        "</ul>";
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

/* Function not in use at the moment. */
function deleteCookie(cname) {
    setCookie(cname, "[DELEATED]", -6355);
}

function openTimeline(day) {
    setCookie('openDay', day, 0.00076);
    openInNewTab('../timeline.xestis.com/index.html'); //http://timeline.xestis.com/
}

function openInNewTab(url) {
    var win = window.open(url, '_self');
    win.focus();
}